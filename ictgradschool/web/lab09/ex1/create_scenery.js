
function checkDophin() {

    $("#toggle-components input").each(function () {

        var name = this.id;
        var index = name.substr(name.length-1, 1);
        var selector = "#dolphin" + index;
        if (this.checked) {
            $(selector).show();
        } else {
            $(selector).hide();
        }
    } )
}


$(function () {

    $("#choose-background input").on("change",function () {
        var name = this.id;
        var index = name.substr(name.length-1, 1);
        var fullname = "";
        if (index == 6) {
            fullname = "../images/background" + index + ".gif";
        } else {
            fullname = "../images/background" + index + ".jpg";
        }
        $("#background")[0].src = fullname;
    });

    $("#toggle-components input").on("change",function () {

        var name = this.id;
        var index = name.substr(name.length-1, 1);
        var selector = "#dolphin" + index;
        console.log($(selector)[0]);
        if (this.checked) {
            $(selector).show();
        } else {
            $(selector).hide();
        }

    } );

    checkDophin();
    $("#slider").slider({
        value: 100,
        slide: function (event, ui) {

            var ratio = ui.value / 100.0;
            var text = "scale" + "(" + ratio + "," + ratio + ")";

            $("#container").css("transform", text);
            // $("#background").css("transform", "scale(ui.value/100.0, ui.value/100.0)");

        }
    });

})